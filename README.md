Modified version of https://github.com/flantel/coreos-mysqlbackup/
Environment variables:

DB_HOST localhost (name_of_linked mysql container to back up)
MAILSERVER: smtp.gmail.com:587 (or any other smtp server with port)
MAILTO: yourname@gmail.com (where to send log-emails)
MAILFROM: automysqlbackup (log senders name)
SMTP_USER: yourname@gmail.com
SMTP_PASS: yourpass
USETLS=YES
USESTARTTLS=YES

link volume: -v /var/mysql_backup_location:/var/lib/automysqlbackup