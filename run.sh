#!/bin/bash

# Create /etc/mysql/debian.cnf using the values
# specified in the environment. 

cat << EOM > /etc/mysql/debian.cnf
[client]
host     = $DB_HOST
user     = root
password = $MYSQL_ROOT_PASSWORD
EOM

# Create the /etc/ssmtp/ssmtp.conf file for sending reports, using 
# values specified in the environment. The below is for a gmail account. Change to suit
# your mail relay
cat << EOM > /etc/ssmtp/ssmtp.conf
root=root
# Example for relaying to Gmail servers
mailhub=$MAILSERVER
AuthUser=$SMTP_USER
AuthPass=$SMTP_PASS
UseTLS=$USETLS
UseSTARTTLS=$USESTARTTLS
EOM


# Testmail
cat << EOM > /msg.txt
TO: $MAILTO
FROM: $MAILFROM
SUBJECT: Mysqlbackup form $DB_HOST
Backup executed
EOM


ssmtp alex@idra.info < /msg.txt 

# The only thing we want this unit to do is run automysqlbackup
# then exit. 
/usr/sbin/automysqlbackup